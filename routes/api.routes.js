module.exports = app => {
    const tutorials = require("../controllers/tutorial.controller.js");
    const categories = require("../controllers/categories.controller.js");
    const products = require("../controllers/products.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", tutorials.create);

    router.post('/categories' , categories.create);

    router.get('/categories' , categories.findAll);
  
    // Retrieve all Tutorials
    router.get("/", tutorials.findAll);
  
    // Retrieve all published Tutorials
    router.get("/published", tutorials.findAllPublished);
  
    // Retrieve a single Tutorial with id
    router.get("/:id", tutorials.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", tutorials.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", tutorials.delete);
    
    router.get("/categories/:id", categories.delete);
  
    // Create a new Tutorial
    router.delete("/", tutorials.deleteAll);
    router.get("/categories/delete/all", categories.deleteAll);


  //Rutas para los productos
  router.post("/products/epicor", products.create);
  router.post('/products/sync', products.syncProcess);
  

  
    app.use('/api', router);
  };