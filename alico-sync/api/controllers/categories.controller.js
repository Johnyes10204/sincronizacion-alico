const db = require("../models");
const Categorie = db.categories;
var Config = require('../config/config')
const axios = require('axios')


function syncCategories(categories) {
    let cat = []
    return new Promise((resolve, reject) => {

        if (categories.name != null) {
            cat.push({ 'name': categories.name.toUpperCase(), 'id': categories.id })
            // Create a Tutorial
            const categorie = new Categorie({
                idmagento: categories.id,
                name: categories.name.toUpperCase(),
            });

            // Save categorie in the database
            categorie
                .save(categorie)
                .then(data => {
                    // res.send(data);
                })
                .catch(err => {
                    // res.status(500).send({
                        // message:
                            // err.message || "Some error occurred while creating the Tutorial."
                    // });
                });
        }


        for (let i = 0; i < categories.children_data.length; i++) {
            //console.log(i,categories.children_data[i].name)
            syncCategories(categories.children_data[i]).then((data) => {
                //console.log(data.length)
                for (let item = 0; item < data.length; item++) {
                    console.log(data[item])
                    cat.push(data[item])
                }
            })

        }


        resolve(cat)
    });
}

// Create and Save a new Tutorial
exports.create = (req, res) => {
    try {
        let url = Config.ecommerce.production + '/categories'
        let token = Config.ecommerce.token;
        let service = {
            method: 'get',
            url: url,
            data: {},
            headers: {
                "Content-Type": "application/json",
                "Authorization": token
            }
        }

        axios(service).then((data) => {
            //console.log('data:',data.data.children_data)
            let categories = data.data
            let response = []
            syncCategories(categories);
            res.json({
                status: categories
            })
            // console.log(categories);

            //return reply(Constants.CATEGORIES)

        }, (error) => {
            console.log('error:', error)
        })
    } catch (error) {
        console.log(error)
    }
    // console.log("hola");
    // Validate request
    if (!req.body.id) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }


};

// Retrieve all Tutorials from the database.
exports.findAll = (req, res) => {
    const title = req.query.title;
    var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

    Categorie.find(condition)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Categorie.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Tutorial with id " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Tutorial with id=" + id });
        });
};

// Update a Tutorial by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Tutorial.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found!`
                });
            } else res.send({ message: "Tutorial was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};

// Delete a Tutorial with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Categorie.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`
                });
            } else {
                res.send({
                    message: "Tutorial was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Tutorial with id=" + id
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
    Categorie.deleteMany({})
        .then(data => {
            res.send({
                message: `${data.deletedCount} Categories were deleted successfully!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all tutorials."
            });
        });
};

// Find all published Tutorials
exports.findAllPublished = (req, res) => {
    Tutorial.find({ published: true })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};