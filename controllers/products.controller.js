const XLSX = require('xlsx');
const db = require("../models");
const Categorie = db.categories;
var Config = require('../config/config')
const axios = require('axios');
var magentoservice = "https://tienda.alico-sa.com/rest/all/V1/integration/admin/token";
var tokendata = "29ohh7h7yq133532m7jkops5sshd8ysq";
// const Config = require("../../config/config");
// const axios = require('axios')
var updateservice = Config.updateserviceProduction;
var getToken = {
    method: 'POST',
    url: magentoservice,
    headers: {
        "Content-Type": "application/json"
    },
    data:{
        "username":"Admin",
        "password": "Exfinity2021*"
    }
}


const convertJsonToExcel = (data) => {
    const workSheet = XLSX.utils.json_to_sheet(data);
    const workBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook,workSheet, "students")
    XLSX.write(workBook,{bookType: 'xlsx', type:'buffer'})
    XLSX.write(workBook,{bookType: 'xlsx', type:'binary'});
    XLSX.writeFile(workBook, "productos.xlsx")
} 

// axios(getToken).then((data) => {
//     console.log("Token controller" + data.data) 
//     tokendata =  data.data;
// });


// console.log(tokendata)


// res.json(tokendata)

var updatesync = {
    method: 'post',
    url: updateservice,
    headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${tokendata}`
    },
    data :""
}

var productsSync = {
    method: "GET",
    url: Config.serviceProducts.url,
    headers: {
        "Content-Type": "application/json",
        'Authorization': `Basic ${Config.serviceProducts.token}`
    }
}


function syncCategories(categories) {
    let cat = []
    return new Promise((resolve, reject) => {

        if (categories.name != null) {
            cat.push({ 'name': categories.name.toUpperCase(), 'id': categories.id })
            // Create a Tutorial
            const categorie = new Categorie({
                idmagento: categories.id,
                name: categories.name.toUpperCase(),
            });

            // Save categorie in the database
            categorie
                .save(categorie)
                .then(data => {
                    // res.send(data);
                })
                .catch(err => {
                    // res.status(500).send({
                    // message:
                    // err.message || "Some error occurred while creating the Tutorial."
                    // });
                });
        }


        for (let i = 0; i < categories.children_data.length; i++) {
            //console.log(i,categories.children_data[i].name)
            syncCategories(categories.children_data[i]).then((data) => {
                //console.log(data.length)
                for (let item = 0; item < data.length; item++) {
                    console.log(data[item])
                    cat.push(data[item])
                }
            })

        }


        resolve(cat)
    });
}

// Create and Save a new Tutorial
exports.create = (req, res) => {
    try {
        let url = Config.ecommerce.production + '/categories'
        let token = Config.ecommerce.token;
        let service = {
            method: 'get',
            url: url,
            data: {},
            headers: {
                "Content-Type": "application/json",
                "Authorization": token
            }
        }

        axios(service).then((data) => {
            //console.log('data:',data.data.children_data)
            let categories = data.data
            let response = []
            syncCategories(categories);
            res.json({
                status: categories
            })
            // console.log(categories);

            //return reply(Constants.CATEGORIES)

        }, (error) => {
            console.log('error:', error)
        })
    } catch (error) {
        console.log(error)
    }
    // console.log("hola");
    // Validate request
    if (!req.body.id) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }


};

// Retrieve all Tutorials from the database.
exports.syncProcess = (req, res) => {

    tokendata = "rm0oa946ch1hm2uzwr7e0xun4rlvmd0m";
    productArray = [];
    let url = Config.ecommerce.production + '/categories'
    const title = req.query.title;
    let service = {
        method: 'get',
        url: url,
        data: {},
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${tokendata}`
        }
    }

    axios(service).then((data) => {
        //console.log('data:',data.data.children_data)
        let categories = data.data
        let response = []
        syncCategories(categories);
        axios(productsSync).then((data) => {

            // return hello;
            // console.log(data)
            mag = data.data;
            mag = mag.value
            console.log(data.data)
            console.log(Object.keys(mag).length)

            // console.log("tamaño" + mag.lenght)
            // for (let i = 0; i < 50; i++) {
            for (let i = 0; i < Object.keys(mag).length; i++) {
                const element = mag[i];
             


                // return ;

                // console.info(i);
                // console.log(mag[i].Part_CommercialCategory);
                // console.log(mag[i].Part_CommercialSubCategory);
                // console.log(element)
                var cat = [];
                var catId = [];
                cat = mag[i].Part_CommercialCategory.split(',')
                cat.push(mag[i].Part_CommercialSubCategory);
                cat.push('TIENDA');
                // console.log(Object.keys(cat).length)
                for (j = 0; j < Object.keys(cat).length; j++) {
                    // console.log(cat[j].trim());
                    for (x = 0; x < categories.length; x++) {
                        if (cat[j].trim() == categories[x].name.trim()) {
                            //console.log('IF',cat[j],categories[x].name,categories[x].id);
                            catId.push(categories[x].idmagento);
                        }

                    }
                }

                // console.log(catId)

                // console.log(mag[i].Part_PartNum),
                // Ancho Part_Number01
                // Alto Part_Number02
                productArray.push({
                    "sku": mag[i].Part_PartNum,
                    "name": mag[i].Part_PartDescription,
                    "price": mag[i].Calculated_PrecioIva,
                    "qty": mag[i].Calculated_Cantidad,
                    "concepto": "SE VENDE",
                    // "proveedor": mag[i].proveedor,
                    // "clase": mag[i].clase,
                    // "estado": mag[i].estado,
                    "custom_attributes": [
                        // { "attribute_code": "mantenimiento_preventivo", "value": mag[i].mantenimiento_preventivo },
                        // { "attribute_code": "caracteristicas_generales", "value": mag[i].caracteristicas_generales },
                        // { "attribute_code": "caracteristicas_tecnicas", "value": mag[i].caracteristicas_tecnicas },
                        // { "attribute_code": "video", "value": mag[i].video },
                        // { "attribute_code": "procedencia", "value": mag[i].procedencia },
                        // { "attribute_code": "peso", "value": mag[i].peso },
                        { "attribute_code": "largo", "value": mag[i].Part_Number02 },
                        { "attribute_code": "ancho", "value": mag[i].Part_Number01 },
                        // { "attribute_code": "alto", "value": mag[i].alto },
                        // { "attribute_code": "unidad_medida_dimensiones", "value": mag[i].unidad_medida_dimensiones },
                        // { "attribute_code": "unidad_medida_peso", "value": mag[i].unidad_medida_peso },
                        // { "attribute_code": "sinonimos", "value": mag[i].sinonimos },
                        // { "attribute_code": "iva", "value": mag[i].iva },
                        // { "attribute_code": "numero_parte", "value": mag[i].numero_parte },
                        { "attribute_code": "short_description", "value": mag[i].Part_DescLongPart_c },
                        { "attribute_code": "description", "value": mag[i].Part_DescLongPart_c},
                        { "attribute_code": "unidad", "value": mag[i].Part_SalesUM},
                        { "attribute_code": "embalaje", "value": mag[i].Part_ConctCaja_c + " / "+ mag[i].Part_ConctPaquete_c },
                        // { "attribute_code": "imagenes", "value": mag[i].imagenes },
                        // { "attribute_code": "lote", "value": mag[i].lote },
                        // { "attribute_code": "brand_name", "value": mag[i].brand_name },
                        // { "attribute_code": "funcion_description", "value": mag[i].funcion_descripcion },
                        { "attribute_code": "category_ids", "value": catId }
                    ]

                })

                // console.log(productArray);
                // console.log(i);


            }
            // console.log(productArray)
            data = { "products": productArray }

            // res.json(data);
            // const output = jsonToXlsx.readAndGet(data);
            convertJsonToExcel(mag);
            // console.log(output)
            // console.log(mag[0].sku, 'data: ', mag[0].categoria)
            // console.log(data)
            updatesync.data = data;
            // console.log(updatesync);
            axios(updatesync).then(function (response) {
                console.log(true, response.data)
                // console.log(new Date())
            }).catch(function (err) {

                
                console.log("failed", err.response.data)
                console.log()
                console.log(new Date())
            })

            // console.log(data)}
            // console.log('categorias', data.data);


        })
        .catch(function (error) {
            console.log(error);
        });
        // res.json({
        //     status: categories
        // })
        // console.log(categories);

        //return reply(Constants.CATEGORIES)

    }, (error) => {
        console.log('error:', error)
    })

};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Categorie.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Tutorial with id " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Tutorial with id=" + id });
        });
};

// Update a Tutorial by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Tutorial.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found!`
                });
            } else res.send({ message: "Tutorial was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};

// Delete a Tutorial with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Categorie.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`
                });
            } else {
                res.send({
                    message: "Tutorial was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Tutorial with id=" + id
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
    Categorie.deleteMany({})
        .then(data => {
            res.send({
                message: `${data.deletedCount} Categories were deleted successfully!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all tutorials."
            });
        });
};

// Find all published Tutorials
exports.findAllPublished = (req, res) => {
    Tutorial.find({ published: true })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};